package com.example.lorenzo.lodealphasports;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class PuntuarFragment extends Fragment implements View.OnClickListener {

    Button guay, meh;
    TextView txtPuntuado;

    public PuntuarFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_puntuar, container, false);

        txtPuntuado = v.findViewById(R.id.txtPuntuar);
        guay = v.findViewById(R.id.btGuay);
        meh = v.findViewById(R.id.btHacostado);
        guay.setOnClickListener(this);
        meh.setOnClickListener(this);
        return v;
    }




    public static PuntuarFragment newInstance() {
        return new PuntuarFragment();
    }

    @Override
    public void onClick(View v) {
        txtPuntuado.setText(R.string.punt_guardada);

    }
}
