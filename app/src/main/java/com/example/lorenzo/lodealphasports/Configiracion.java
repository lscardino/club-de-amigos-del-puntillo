package com.example.lorenzo.lodealphasports;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import java.util.Locale;

public class Configiracion extends AppCompatActivity {

    Button espa, ingle, portu, cata;
    Animation animation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configiracion);

        espa = findViewById(R.id.btEspanyol);
        ingle = findViewById(R.id.bteng);
        portu = findViewById(R.id.btpt);
        cata = findViewById(R.id.btcat);

        animation = AnimationUtils.loadAnimation(Configiracion.this, R.anim.blink_anim);


    }

    private void setAppLocale(String localeCode){
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(localeCode.toLowerCase()));

        res.updateConfiguration(conf,dm);


    }

    public void clearAnim(){
        espa.clearAnimation();
        portu.clearAnimation();
        ingle.clearAnimation();
        cata.clearAnimation();
    }


    public void LocalEsp(View view) {
        clearAnim();
        setAppLocale("es");
        espa.startAnimation(animation);
        Toast noContra =  Toast.makeText(this,R.string.textoESp,
                Toast.LENGTH_SHORT);
        noContra.show();
    }

    public void LocalPt(View view) {
        clearAnim();
        setAppLocale("pt");
        portu.startAnimation(animation);
        Toast noContra =  Toast.makeText(this,R.string.textoApt,
                Toast.LENGTH_SHORT);
        noContra.show();
    }

    public void LocalEng(View view) {
        clearAnim();
        setAppLocale("en");
        ingle.startAnimation(animation);
        Toast noContra =  Toast.makeText(this,R.string.textaEng,
                Toast.LENGTH_SHORT);
        noContra.show();
    }

    public void LocalCat(View view) {
        clearAnim();
        setAppLocale("ca");
        cata.startAnimation(animation);
        Toast noContra =  Toast.makeText(this,R.string.textoACat,
                Toast.LENGTH_SHORT);
        noContra.show();
    }

    public void Voltar(View view) {
        Intent intent = new Intent(this,menu_principal.class);
        startActivity(intent);
    }
}
