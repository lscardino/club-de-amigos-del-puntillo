package com.example.lorenzo.lodealphasports;

import java.util.HashMap;

public class DatosUsuarios {

    static String[] users_id = {"User1","Nicaya","Infa123","ExO_o","Sotyka94",
            "UraniumCookies","thenicob","ADcMacDonalds"};
    static String[] nombres_users = {"Guillermo Alvarez", "Marta Amiel", "Raul Molina",
            "Paco Sanchez","Ana Anna","Fererico Mendez","Raymundo Ruiz",
            "Fernando Fernandez"};
    static String[] passwd = {"contra1","contra2","contra3","contra4",
            "contra5","contra6","contra7","contra8"};
    static int[] edad_users = {23,45,45,19,21,32,22,34};
    static int imagenes = R.drawable.foto_users;

    //Es ineficiente, user_id[] y passwd[] deberian ir aqui en un HashMap.
    static HashMap<String,String> datosConj = new HashMap<>();


    public static void iniciarDatos(){
        for (int i = 0; i<users_id.length;i++) {
            datosConj.put(users_id[i],passwd[i]);
        }
    }
}
