package com.example.lorenzo.lodealphasports;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Ejercicio_Seleccionado extends AppCompatActivity {


    public boolean seHaMostrado;
    TextView verVideo,txtPuntuar;
    Button puntuar, volver;
    ImageButton btVideo;
    boolean isFragmentDisplayed;
    public String compartir,cnQuien;

    static final String ESTADO_FRAGMENT = "Estado_del_fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_ejercicio__seleccionado);

        seHaMostrado = false;
        verVideo = findViewById(R.id.txtVerVideo);
        puntuar = findViewById(R.id.btPuntuar);
        volver = findViewById(R.id.btnVolver);
        btVideo = findViewById(R.id.ibPlay);

        compartir = this.getResources().getString(R.string.textoAcompartir);
        cnQuien = this.getResources().getString(R.string.cnQuien);


        if (savedInstanceState != null) {
            isFragmentDisplayed =
                    savedInstanceState.getBoolean(ESTADO_FRAGMENT);
            if (isFragmentDisplayed) {
                puntuar.setText(R.string.btVolver);
            }

        }
    }

    public void abrirPuntuar(View view) {
        if (!isFragmentDisplayed){
            displayFragment();
            verVideo.setVisibility(View.GONE);
            btVideo.setVisibility(View.GONE);
            volver.setVisibility(View.GONE);
            puntuar.setText(R.string.btVolver);
        }else{
            closeFragment();
            verVideo.setVisibility(View.VISIBLE);
            btVideo.setVisibility(View.VISIBLE);
            volver.setVisibility(View.VISIBLE);
            puntuar.setText(R.string.btPuntuar);
            if (!seHaMostrado){
                seHaMostrado = true;
                final AlertDialog.Builder builder = new AlertDialog.Builder(Ejercicio_Seleccionado.this);
                builder.setMessage(R.string.quieresCompartir)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Claro, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String mimeType = "text/plain";
                                ShareCompat.IntentBuilder
                                        .from(Ejercicio_Seleccionado.this)
                                        .setType(mimeType)
                                        .setChooserTitle(R.string.cnQuien)
                                        .setText(compartir)
                                        .startChooser();
                            }
                        })
                        .setNegativeButton(R.string.Nope, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog titulo = builder.create();
                titulo.setTitle(R.string.share);
                titulo.show();

            }
            //Dialog aqui
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the state of the fragment (true=open, false=closed).
        savedInstanceState.putBoolean(ESTADO_FRAGMENT, isFragmentDisplayed);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void displayFragment(){
        PuntuarFragment puntuarFragment = PuntuarFragment.newInstance();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.fragment_container, puntuarFragment).addToBackStack(null).commit();

        isFragmentDisplayed = true;

    }

    public void closeFragment() {
        // Get the FragmentManager.
        FragmentManager fragmentManager = getSupportFragmentManager();
        // Check to see if the fragment is already showing.
        PuntuarFragment puntuarFragment = (PuntuarFragment) fragmentManager
                .findFragmentById(R.id.fragment_container);
        if (puntuarFragment != null) {
            // Create and commit the transaction to remove the fragment.
            FragmentTransaction fragmentTransaction =
                    fragmentManager.beginTransaction();
            fragmentTransaction.remove(puntuarFragment).commit();
        }
        isFragmentDisplayed = false;
    }

    public void PreguntaVideo(View view) {
        Intent intent = new Intent(this,ReproducirVideo.class);
        startActivity(intent);
    }

    public void Volver(View view) {
        finish();
    }
}
