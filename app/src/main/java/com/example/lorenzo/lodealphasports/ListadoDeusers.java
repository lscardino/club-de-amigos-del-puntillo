package com.example.lorenzo.lodealphasports;

import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lorenzo.lodealphasports.BaseLocal.UserDataSource;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ListadoDeusers extends AppCompatActivity {

    private ListView lstUser;
    private FloatingActionButton fab;
    public String editar,mensajeEditar,delete,deleteRuShure, borrarUno;


    List<Usuario> userList = new ArrayList<>();
    ArrayAdapter adapter;

    private CompositeDisposable compositeDisposable;
    private UsersRepository usersRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_users_db);

        compositeDisposable = new CompositeDisposable();

        editar = this.getResources().getString(R.string.editar);
        mensajeEditar = this.getResources().getString(R.string.msgEditar);
        delete = this.getResources().getString(R.string.borrarTodo);
        deleteRuShure = this.getResources().getString(R.string.msgDelete);
        borrarUno = this.getResources().getString(R.string.seguroBorrar);



        lstUser = findViewById(R.id.Firstuser);
        fab = findViewById(R.id.botonFaborito);

        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,userList);
        registerForContextMenu(lstUser);
        lstUser.setAdapter(adapter);

        UsersRoomDatabase userDataBase = UsersRoomDatabase.getINSTANCE(this);
        usersRepository = UsersRepository.getmInstance(UserDataSource.getInstance(userDataBase.usuariosDao()));

        loadData();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }

    private void loadData() {
        Disposable disposable = usersRepository.getAllUsers().
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe(new Consumer<List<Usuario>>() {
            @Override
            public void accept(List<Usuario> usuarios) throws Exception {
                onGetAllUsersSucces(usuarios);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Toast.makeText(ListadoDeusers.this,
                        ""+throwable.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
        compositeDisposable.add(disposable);
    }

    private void onGetAllUsersSucces(List<Usuario> usuarios){
        userList.clear();
        userList.addAll(usuarios);
        adapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_users,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_users:
                BorrarTutiUser();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteAllUsers(){
        Disposable disposable = io.reactivex.Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                usersRepository.deleteAll();
                emitter.onComplete();
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer() {
                               @Override
                               public void accept(Object o) throws Exception {

                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {
                                   Toast.makeText(ListadoDeusers.this,
                                           "" + throwable.getMessage(),
                                           Toast.LENGTH_SHORT).show();
                               }
                           }, new Action() {
                               @Override
                               public void run() throws Exception {
                                   loadData();
                               }
                           }

                );
        compositeDisposable.add(disposable);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v , menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        menu.setHeaderTitle(R.string.selecciona_accion);

        menu.add(Menu.NONE, 0, Menu.NONE ,"UPDATE");
        menu.add(Menu.NONE, 1, Menu.NONE ,"DELETE");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Usuario user = userList.get(info.position);

        switch (item.getItemId()) {
            case 0: {
                final EditText editTextName = new EditText(ListadoDeusers.this);
                editTextName.setText(user.getNombre());
                editTextName.setHint(R.string.nuevoNombre);
                new AlertDialog.Builder(ListadoDeusers.this)
                        .setTitle(editar)
                        .setMessage(mensajeEditar)
                        .setView(editTextName)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (TextUtils.isEmpty(editTextName.getText().toString())) {
                                    return;
                                } else {
                                    user.setNombre(editTextName.getText().toString());
                                    updateUser(user);
                                }
                            }
                        }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
                break;
            }


            case 1: {
                new AlertDialog.Builder(ListadoDeusers.this)
                        .setMessage(borrarUno + user.getNombre() + "?")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteUser(user);

                            }
                        }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
                break;
            }
        }
        return true;
    }

    private void deleteUser(final Usuario user) {
        Disposable disposable = io.reactivex.Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                usersRepository.deleteUsers(user);
                emitter.onComplete();
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer() {
                               @Override
                               public void accept(Object o) throws Exception {

                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {
                                   Toast.makeText(ListadoDeusers.this,
                                           "" + throwable.getMessage(),
                                           Toast.LENGTH_SHORT).show();
                               }
                           }, new Action() {
                               @Override
                               public void run() throws Exception {
                                   loadData();
                               }
                           }

                );
        compositeDisposable.add(disposable);

    }

    private void updateUser(final Usuario user) {
        Disposable disposable = io.reactivex.Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                usersRepository.updateUser(user);
                emitter.onComplete();
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer() {
                               @Override
                               public void accept(Object o) throws Exception {

                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {
                                   Toast.makeText(ListadoDeusers.this,
                                           "" + throwable.getMessage(),
                                           Toast.LENGTH_SHORT).show();
                               }
                           }, new Action() {
                               @Override
                               public void run() throws Exception {
                                   loadData();
                               }
                           }

                );
        compositeDisposable.add(disposable);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        compositeDisposable.clear();
    }


    public void BorrarTutiUser(){
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ListadoDeusers.this);
        builder.setMessage(deleteRuShure)
                .setCancelable(false)
                .setPositiveButton(R.string.Claro, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAllUsers();
                    }
                })
                .setNegativeButton(R.string.Nope, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        android.app.AlertDialog titulo = builder.create();
        titulo.setTitle(delete);
        titulo.show();

    }


}
