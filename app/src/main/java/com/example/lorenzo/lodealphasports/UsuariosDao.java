package com.example.lorenzo.lodealphasports;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public interface UsuariosDao {

    @Insert
    void insert(Usuario user);

    @Delete
    void deleteUsers(Usuario user);

    @Update
    void updateUser(Usuario user);

    @Query("DELETE FROM tabla_usuarios")
    void deleteAll();

   @Query("SELECT * from tabla_usuarios ORDER BY userId ASC")
   Flowable<List<Usuario>> getAllUsers();

    @Query("SELECT * FROM tabla_usuarios WHERE userId= :IdUsuario")
    //Usuario getUserById(String IdUsuario);
    Usuario getUserById(String IdUsuario);
    //LiveData<List<Usuario>> getAllUsers();


}
