package com.example.lorenzo.lodealphasports;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorListaUsers extends RecyclerView.Adapter<AdaptadorListaUsers.MyViewHolder> {

    private ArrayList<ModeloUsuarios> datamodel;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtNombre;
        TextView txtUser;
        ImageView imIcono;


        public MyViewHolder(View itemView){
            super(itemView);
            this.txtNombre = (TextView) itemView.findViewById(R.id.txtNombreUser);
            this.txtUser = (TextView) itemView.findViewById(R.id.txtIdUser);
            this.imIcono = (ImageView) itemView.findViewById(R.id.imgUsuario);
        }

    }

    public AdaptadorListaUsers(ArrayList<ModeloUsuarios> datos){
        this.datamodel = datos;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.items_usuarios,viewGroup,false);
        MyViewHolder nvh = new MyViewHolder(v);
        return nvh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        //Creo que habrá error
        //ModeloUsuarios actual = datamodel.get(i);
        myViewHolder.txtNombre.setText(datamodel.get(i).getNombre());
        myViewHolder.txtUser.setText(datamodel.get(i).getUsername());
        myViewHolder.imIcono.setImageResource(datamodel.get(i).getImagen());
    }

    @Override
    public int getItemCount() {
        return datamodel.size();
    }
}
