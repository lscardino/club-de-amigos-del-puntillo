package com.example.lorenzo.lodealphasports.BaseLocal;

import com.example.lorenzo.lodealphasports.Usuario;

import java.util.List;

public interface DataBaseCallBack {

    void onUsersLoaded(List<Usuario> users);

    void onUserDeleted();

    void onUserAdded();

    void onDataNotAvailable();

    void onUserUpdated();
}
