package com.example.lorenzo.lodealphasports;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "tabla_usuarios")

public class Usuario {
    private String nombre;
    private String apellidos;
    private String email;

    @PrimaryKey
    @NonNull
    private String userId;

    private String contra;

    public Usuario(){}

    @Ignore
    public Usuario(@NonNull String nombre , @NonNull String apellidos,
                   @NonNull String email, @NonNull String userId ,
                   @NonNull String contra){
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.userId = userId;
        this.contra = contra;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getEmail() {
        return email;
    }

    public String getUserId() {
        return userId;
    }

    public String getContra() {
        return contra;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setContra(String contra) {
        this.contra = contra;
    }

    @Override
    public  String toString(){
        return new StringBuilder(nombre).append("\n").append(email).toString();
    }
}
