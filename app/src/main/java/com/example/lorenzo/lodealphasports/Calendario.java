package com.example.lorenzo.lodealphasports;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.telecom.Call;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CalendarView;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.SimpleTimeZone;

public class Calendario extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BetaFragmente.OnFragmentInteractionListener,
        GammaFragment.OnFragmentInteractionListener,
        AlphaFragment.OnFragmentInteractionListener {

    public String compartir, cnQuien;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendario);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        compartir = this.getResources().getString(R.string.textoAcompartir);
        cnQuien = this.getResources().getString(R.string.cnQuien);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.calendario, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;
        Boolean fragmentoSeleccionado = false;


        if (id == R.id.cmbiarAlpha) {

            fragment = new AlphaFragment();
            fragmentoSeleccionado = true;

        } else if (id == R.id.cmbiarBeta) {
            fragment = new BetaFragmente();
            fragmentoSeleccionado = true;

        } else if (id == R.id.cmbiarGamma) {
            fragment = new GammaFragment();
            fragmentoSeleccionado = true;

        } else if (id == R.id.nav_manage) {
            Intent intent = new Intent(this,Configiracion.class);
            startActivity(intent);

        } else if (id == R.id.nav_share) {

            String url = "https://campus.copernic.cat/";
            Uri webpage = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
            if (intent.resolveActivity(getPackageManager()) !=null){
                startActivity(intent);
            }

        } else if (id == R.id.nav_send) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(Calendario.this);
            builder.setMessage(R.string.quieresCompartir)
                    .setCancelable(false)
                    .setPositiveButton(R.string.Claro, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String mimeType = "text/plain";
                            ShareCompat.IntentBuilder
                                    .from(Calendario.this)
                                    .setType(mimeType)
                                    .setChooserTitle(cnQuien)
                                    .setText(compartir)
                                    .startChooser();
                        }
                    })
                    .setNegativeButton(R.string.Nope, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog titulo = builder.create();
            titulo.setTitle(R.string.share);
            titulo.show();

        }


        if (fragmentoSeleccionado){
            getSupportFragmentManager().beginTransaction().replace(R.id.LayoutDelCalendario, fragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void empezarEjer(View view) {
        Intent intent = new Intent(this,Ejercicio_Seleccionado.class);
        startActivity(intent);
    }
}
