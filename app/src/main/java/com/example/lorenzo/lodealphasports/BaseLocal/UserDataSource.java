package com.example.lorenzo.lodealphasports.BaseLocal;

import com.example.lorenzo.lodealphasports.BaseDatos.IUserDataSource;
import com.example.lorenzo.lodealphasports.Usuario;
import com.example.lorenzo.lodealphasports.UsuariosDao;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

public class UserDataSource implements IUserDataSource {

    private UsuariosDao userDao;
    private static UserDataSource mInstance;

    public UserDataSource(UsuariosDao userDao) {
        this.userDao = userDao;
    }


    public static UserDataSource getInstance(UsuariosDao userDao){
        if (mInstance == null){
            mInstance = new UserDataSource(userDao);
        }
        return mInstance;

    }



    @Override
    public void insert(Usuario user) {
        userDao.insert(user);

    }

    @Override
    public void deleteUsers(Usuario user) {
        userDao.deleteUsers(user);
    }

    @Override
    public void updateUser(Usuario user) {
        userDao.updateUser(user);
    }

    @Override
    public void deleteAll() {
        userDao.deleteAll();
    }

    @Override
    public Usuario getUserById(String IdUsuario) {
        return userDao.getUserById(IdUsuario);
    }

    @Override
    public Flowable<List<Usuario>> getAllUsers() {
        return userDao.getAllUsers();
    }
}
