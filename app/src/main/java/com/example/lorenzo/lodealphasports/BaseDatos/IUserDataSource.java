package com.example.lorenzo.lodealphasports.BaseDatos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.lorenzo.lodealphasports.Usuario;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

public interface IUserDataSource {
        void insert(Usuario user);
        void deleteUsers(Usuario user);
        void updateUser(Usuario user);
        void deleteAll();
        Usuario getUserById(String IdUsuario);
        Flowable<List<Usuario>> getAllUsers();

    }
