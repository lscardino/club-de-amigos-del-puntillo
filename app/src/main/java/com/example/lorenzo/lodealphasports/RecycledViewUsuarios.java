package com.example.lorenzo.lodealphasports;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class RecycledViewUsuarios extends AppCompatActivity  {

    private RecyclerView mRecycledViewU;
    private RecyclerView.Adapter mAdapterU;
    private RecyclerView.LayoutManager mLayoutManagerU;
    private static ArrayList<ModeloUsuarios> usersArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycled_view_usuarios);

        usersArrayList = new ArrayList<ModeloUsuarios>();
        for (int i = 0; i< DatosUsuarios.nombres_users.length; i++){
            usersArrayList.add(new ModeloUsuarios(
                    DatosUsuarios.users_id[i],
                    DatosUsuarios.nombres_users[i],
                    DatosUsuarios.edad_users[i],
                    DatosUsuarios.imagenes
            ));
        }

        mRecycledViewU = findViewById(R.id.RecViewListaUsers);
        mRecycledViewU.setHasFixedSize(true);
        mLayoutManagerU = new LinearLayoutManager(this);
        mAdapterU = new AdaptadorListaUsers(usersArrayList);

        mRecycledViewU.setLayoutManager(mLayoutManagerU);
        mRecycledViewU.setAdapter(mAdapterU);

    }

    public void Tornar(View view) {
        finish();
    }


}
