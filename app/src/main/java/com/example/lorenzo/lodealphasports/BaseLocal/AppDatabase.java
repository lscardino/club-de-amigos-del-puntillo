package com.example.lorenzo.lodealphasports.BaseLocal;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.lorenzo.lodealphasports.Usuario;
import com.example.lorenzo.lodealphasports.UsuariosDao;

@Database(entities = {Usuario.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UsuariosDao usuariosDao();
}