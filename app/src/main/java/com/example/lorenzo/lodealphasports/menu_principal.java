package com.example.lorenzo.lodealphasports;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

public class menu_principal extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG =
            menu_principal.class.getSimpleName();

    ImageButton btSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        btSalir = findViewById(R.id.ibLogOut);
        btSalir.setOnClickListener(menu_principal.this);
    }


    public void listaDeUsers(View view){
        Log.d(LOG_TAG, "Exito");
        Intent intent = new Intent(this,ListadoDeusers.class);
        startActivity(intent);
    }

    public void abrirCalendario(View view){
        Log.d(LOG_TAG, "Exito");
        Intent intent = new Intent(this,PlanesRView.class);
        startActivity(intent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    public void configuracion(View view) {

        Intent intent = new Intent(this,Configiracion.class);
        startActivity(intent);
    }
}
