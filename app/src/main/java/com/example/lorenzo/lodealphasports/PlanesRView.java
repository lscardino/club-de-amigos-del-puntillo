package com.example.lorenzo.lodealphasports;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class PlanesRView extends AppCompatActivity implements AdaptadorRecycled.OnNoteListener {

    private RecyclerView mRecycledView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayaoutManager;
    private LinearLayout linera;
    ArrayList<ItemsCard> listaCards = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planes_rview);
        linera = findViewById(R.id.layoutCarta);

        listaCards.add(new ItemsCard(R.drawable.ic_date_range_white,"ALPHA",getString(R.string.completadoMedio),linera));
        listaCards.add(new ItemsCard(R.drawable.ic_date_range_white,"BETA",getString(R.string.completadoPoco),linera));
        listaCards.add(new ItemsCard(R.drawable.ic_date_range_white,"GAMMA",getString(R.string.sinCompletar),linera));
        listaCards.add(new ItemsCard(R.drawable.ic_date_range_white,"DELTA",getString(R.string.sinCompletar),linera));
        listaCards.add(new ItemsCard(R.drawable.ic_lock_plan,"EPSILON",getString(R.string.planbloqueado),linera));
        listaCards.add(new ItemsCard(R.drawable.ic_lock_plan,"DSETA",getString(R.string.planbloqueado),linera));
        listaCards.add(new ItemsCard(R.drawable.ic_lock_plan,"ETA",getString(R.string.planbloqueado),linera));
        listaCards.add(new ItemsCard(R.drawable.ic_lock_plan,"ZETA",getString(R.string.planbloqueado),linera));
        listaCards.add(new ItemsCard(R.drawable.ic_lock_plan,"IOTA",getString(R.string.planbloqueado),linera));
        listaCards.add(new ItemsCard(R.drawable.ic_lock_plan,"KAPPA",getString(R.string.planbloqueado),linera));
        listaCards.add(new ItemsCard(R.drawable.ic_lock_plan,"LAMBDA",getString(R.string.planbloqueado),linera));





        mRecycledView = findViewById(R.id.RecViewProgramas);
        //Solo si sabes que el RV no cambiará de tamaño
        mRecycledView.setHasFixedSize(true);
        mLayaoutManager = new LinearLayoutManager(this);
        mAdapter = new AdaptadorRecycled(listaCards,this);

        mRecycledView.setLayoutManager(mLayaoutManager);
        mRecycledView.setAdapter(mAdapter);

    }

    public void volver(View view) {
        finish();
    }

    @Override
    public void OnNoteClick(int posicion) {
        listaCards.get(posicion);
        if (posicion == 0){
            Intent intent = new Intent(this,Calendario.class);
            startActivity(intent);
        }


    }
}
