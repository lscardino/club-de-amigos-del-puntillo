package com.example.lorenzo.lodealphasports;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AdaptadorUsuariosDataBase extends RecyclerView.Adapter<AdaptadorUsuariosDataBase.UserDBViewHolder> {

        private final LayoutInflater mInflater;
        private List<Usuario> mUsers;

    AdaptadorUsuariosDataBase(Context context) { mInflater = LayoutInflater.from(context); }

        @Override
        public UserDBViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = mInflater.inflate(R.layout.items_usuarios, parent, false);
            return new UserDBViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(UserDBViewHolder holder, int position) {
            if (mUsers != null) {
                Usuario current = mUsers.get(position);
                holder.wordItemView.setText(current.getUserId());
            } else {
                // Covers the case of data not being ready yet.
                holder.wordItemView.setText(R.string.notUSersYet);
            }
        }

        void setWords(List<Usuario> words){
            mUsers = words;
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            if (mUsers != null)
                return mUsers.size();
            else return 0;
        }

        class UserDBViewHolder extends RecyclerView.ViewHolder {
            private final TextView wordItemView;

            private UserDBViewHolder(View itemView) {
                super(itemView);
                wordItemView = itemView.findViewById(R.id.textView);
            }
        }
    }

