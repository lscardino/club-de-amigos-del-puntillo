package com.example.lorenzo.lodealphasports;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import static com.example.lorenzo.lodealphasports.R.color.BETA;

public class AdaptadorRecycled extends RecyclerView.Adapter<AdaptadorRecycled.CardViewHolder>{

    private ArrayList<ItemsCard> mItemList;
    private OnNoteListener MonNoteListener;

    public static class CardViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView mImagen;
        public TextView txtnom;
        public TextView txtdurad;
        public LinearLayout layoutCarta;
        public OnNoteListener onNoteListener;

        public CardViewHolder(@NonNull View itemView,OnNoteListener onNoteListener) {
            super(itemView);
            mImagen = itemView.findViewById(R.id.imgVCardPlan);
            txtnom = itemView.findViewById(R.id.txtNomPlan);
            txtdurad = itemView.findViewById(R.id.txtTiempoPlan);
            layoutCarta = itemView.findViewById(R.id.layoutCarta);
            this.onNoteListener = onNoteListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onNoteListener.OnNoteClick(getAdapterPosition());
        }
    }

    public AdaptadorRecycled(ArrayList<ItemsCard> itemsCartas, OnNoteListener onNoteListener){
        mItemList = itemsCartas;
        this.MonNoteListener = onNoteListener;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cartas_programa, viewGroup, false);
       CardViewHolder cvh = new CardViewHolder(v,MonNoteListener);
       return cvh;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull CardViewHolder cardViewHolder, int i) {

        ItemsCard ItemActual = mItemList.get(i);


        cardViewHolder.mImagen.setImageResource(ItemActual.getImagenCarta());
        cardViewHolder.txtnom.setText(ItemActual.getNomPlan());
        cardViewHolder.txtdurad.setText(ItemActual.getCompPlan());
        if(i == 0) {
            cardViewHolder.layoutCarta.setBackgroundColor(0xFFE65100);
        }else if (i == 1){
            //cardViewHolder.itemView.setBackgroundColor(Color.GREEN);
            cardViewHolder.layoutCarta.setBackgroundColor(0xFF3B5ED9);
        }else if(i == 2) {
            cardViewHolder.layoutCarta.setBackgroundColor(0xFF00574B);
            //cardViewHolder.itemView.setBackgroundColor(Color.RED);
        }else if(i == 3){
                cardViewHolder.layoutCarta.setBackgroundColor(0xFFD59F3D);
        }else{
            cardViewHolder.layoutCarta.setBackgroundColor(0xFF807E7E);
        }
        /*
        }else if(i == 4){
            cardViewHolder.layoutCarta.setBackgroundColor(0xFFE91F15);
        }else if(i == 5){
            cardViewHolder.layoutCarta.setBackgroundColor(0xFFD59F3D);
        }else if(i == 6){
            cardViewHolder.layoutCarta.setBackgroundColor(0xFF009371);
        }else if(i == 7){
            cardViewHolder.layoutCarta.setBackgroundColor(0xFF0093F1);
        }else if(i == 8){
            cardViewHolder.layoutCarta.setBackgroundColor(0xFF003E66);
        }else if(i == 9){
            cardViewHolder.layoutCarta.setBackgroundColor(0xFFA01449);
        }else if(i == 10){
            cardViewHolder.layoutCarta.setBackgroundColor(0xFF7C1D1D);
        }

        /**/
}

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    public interface OnNoteListener{
        void    OnNoteClick(int posicion);
    }
}
