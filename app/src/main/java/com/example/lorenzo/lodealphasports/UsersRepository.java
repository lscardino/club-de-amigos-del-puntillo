package com.example.lorenzo.lodealphasports;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.lorenzo.lodealphasports.BaseDatos.IUserDataSource;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

public class UsersRepository implements IUserDataSource {

    private IUserDataSource mLocalDataSource;

    private static UsersRepository mInstance;

    public UsersRepository(IUserDataSource mLocalDataSource) {
        this.mLocalDataSource = mLocalDataSource;
    }

    public static UsersRepository getmInstance(IUserDataSource mLocalDataSource){
        if (mInstance == null){
            mInstance = new UsersRepository(mLocalDataSource);
        }
        return mInstance;
    }

    @Override
    public void insert(Usuario user) {
        mLocalDataSource.insert(user);
    }

    @Override
    public void deleteUsers(Usuario user) {
        mLocalDataSource.deleteUsers(user);
    }

    @Override
    public void updateUser(Usuario user) {
        mLocalDataSource.updateUser(user);
    }

    @Override
    public void deleteAll() {
        mLocalDataSource.deleteAll();
    }

    @Override
    public Usuario getUserById(String IdUsuario) {
        return mLocalDataSource.getUserById(IdUsuario);
    }

    @Override
    public Flowable<List<Usuario>> getAllUsers() {
        return mLocalDataSource.getAllUsers();
    }
}
