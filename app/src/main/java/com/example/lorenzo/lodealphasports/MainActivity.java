package com.example.lorenzo.lodealphasports;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lorenzo.lodealphasports.BaseLocal.UserDataSource;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.UUID;

import io.reactivex.Maybe;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends AppCompatActivity  {


    private static final String LOG_TAG =
            MainActivity.class.getSimpleName();

    private EditText usuario;
    private EditText passwd;
    private HashMap<String,String> datosLogin = new HashMap<>();

    private static CompositeDisposable compositeDisposable;
    private static UsersRepository usersRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DatosUsuarios.iniciarDatos();
        datosLogin = DatosUsuarios.datosConj;
        compositeDisposable = new CompositeDisposable();
        UsersRoomDatabase userDataBase = UsersRoomDatabase.getINSTANCE(this);
        usersRepository = UsersRepository.getmInstance(UserDataSource.getInstance(userDataBase.usuariosDao()));
    }


    public void abrirMenu(View view) {
        usuario = findViewById(R.id.eTxtUsername);
        passwd = findViewById(R.id.eTxtPassword);
        Intent intent = new Intent(this, menu_principal.class);
        if (usuario.getText().toString().isEmpty() || passwd.getText().toString().isEmpty()) {
            Toast camposVacios = Toast.makeText(this, R.string.camposVacios,
                    Toast.LENGTH_SHORT);
            camposVacios.show();
        } else {
            Usuario user = usersRepository.getUserById(usuario.getText().toString());
            if (user == null) {
                Toast noContra = Toast.makeText(this, R.string.userInexistente,
                        Toast.LENGTH_SHORT);
                noContra.show();
            } else {
                if (user.getContra().equals(passwd.getText().toString())) {
                    startActivity(intent);
                } else {
                    Toast contraMal = Toast.makeText(this, R.string.contrIncorrecta,
                            Toast.LENGTH_SHORT);
                    contraMal.show();
                }
            }
        }
    }

    public void contraOlvidada(View view){
        Toast noContra =  Toast.makeText(this,R.string.cOlvidadaToast,
                        Toast.LENGTH_SHORT);
        noContra.show();
    }

    public void negarRegistro(View view){
        Intent intent = new Intent(this, Registro.class);
        startActivity(intent);
    }




}
