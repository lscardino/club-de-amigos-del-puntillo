package com.example.lorenzo.lodealphasports;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {Usuario.class}, version = 1, exportSchema = false)
public abstract class UsersRoomDatabase extends RoomDatabase {

    public abstract UsuariosDao usuariosDao();

    private static UsersRoomDatabase INSTANCE;

    static UsersRoomDatabase getINSTANCE(final Context context){
        if (INSTANCE == null){
            synchronized (UsersRoomDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext()
                            ,UsersRoomDatabase.class, "usuarios_database").
                            fallbackToDestructiveMigration().allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCE;
    }

    //Lo que hay abajo el tio no lo ha pueso


    /*
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){

        public void onOpen (@NonNull SupportSQLiteDatabase db){
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };



    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final UsuariosDao mDao;
        Usuario user1 = new Usuario("Paco","Perez","email@hotmail.com","Pacops","contra1");
        Usuario[] users = {user1};

        PopulateDbAsync(UsersRoomDatabase db) {
            mDao = db.usuariosDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            // Start the app with a clean database every time.
            // Not needed if you only populate the database
            // when it is first created
            mDao.deleteAll();

            //for (int i = 0; i <= users.length - 1; i++) {
                //Usuario word = new Usuario(users[i]);
                mDao.insert(user1);
            //}
            return null;
        }
    }
    */

}
