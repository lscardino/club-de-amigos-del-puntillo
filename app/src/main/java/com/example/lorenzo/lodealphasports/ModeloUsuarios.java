package com.example.lorenzo.lodealphasports;

import java.util.ArrayList;
import java.util.List;

public class ModeloUsuarios {
     String  username;;
     String nombre;
     int edad;
     int imagen;

    ModeloUsuarios(String username,String nombre, int edad, int imagen){
        this.username = username;
        this.edad = edad;
        this.nombre = nombre;
        this.imagen = imagen;
    }


    public String getUsername() {
        return username;
    }


    public String getNombre() {
        return nombre;
    }

    public  int getEdad() {
        return edad;
    }

    public int getImagen() {
        return imagen;
    }
}
