package com.example.lorenzo.lodealphasports;

import android.text.Layout;
import android.widget.LinearLayout;

public class ItemsCard {
    private int imagenCarta;
    private String nomPlan;
    private String compPlan;
    private LinearLayout layout;

    public ItemsCard(int imagenCarta, String nomPlan, String compPlan, LinearLayout layout){
        this.imagenCarta = imagenCarta;
        this.nomPlan = nomPlan;
        this.compPlan = compPlan;
    }

    public int getImagenCarta() {
        return imagenCarta;
    }

    public String getNomPlan() {
        return nomPlan;
    }

    public String getCompPlan() {
        return compPlan;
    }

    public LinearLayout getLayout() {
        return layout;
    }
}
