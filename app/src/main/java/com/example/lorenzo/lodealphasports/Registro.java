package com.example.lorenzo.lodealphasports;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lorenzo.lodealphasports.BaseLocal.UserDataSource;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class Registro extends AppCompatActivity {

    private ListView lstUser;
    private FloatingActionButton fab;

    public EditText nombre, apellidos, email, contra, repContra, iduser;

    List<Usuario> userList = new ArrayList<>();
    ArrayAdapter adapter;

    private CompositeDisposable compositeDisposable;
    private UsersRepository usersRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        nombre = findViewById(R.id.etxtNomReg);
        apellidos = findViewById(R.id.etxtApellidosReg);
        email = findViewById(R.id.etxtEmailReg);
        contra = findViewById(R.id.etxtContraReg);
        repContra = findViewById(R.id.etxtRepContraReg);
        iduser = findViewById(R.id.etxtUserReg);


        compositeDisposable = new CompositeDisposable();

        //adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,userList);
        //registerForContextMenu(lstUser);
        //lstUser.setAdapter(adapter);

        UsersRoomDatabase userDataBase = UsersRoomDatabase.getINSTANCE(this);
        usersRepository = UsersRepository.getmInstance(UserDataSource.getInstance(userDataBase.usuariosDao()));

        //loadData();

    }

    public void SalirMP(View view) {
        finish();
    }

    public void EnviarReg(View view) {
        if (nombre.getText().toString().isEmpty()|| apellidos.getText().toString().isEmpty()||
                email.getText().toString().isEmpty() || contra.getText().toString().isEmpty()||
                repContra.getText().toString().isEmpty() || iduser.getText().toString().isEmpty()) {
            Toast camposVacios = Toast.makeText(this, R.string.camposVacios,
                    Toast.LENGTH_SHORT);
            camposVacios.show();
        }else{
            if (contra.getText().toString().equals(repContra.getText().toString())) {

                Disposable disposable = io.reactivex.Observable.create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                        Usuario user = new Usuario(nombre.getText().toString(),apellidos.getText().toString(),
                                email.getText().toString(),iduser.getText().toString(),contra.getText().toString());
                        userList.add(user);
                        usersRepository.insert(user);
                        emitter.onComplete();
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer() {
                                       @Override
                                       public void accept(Object o) throws Exception {
                                           Toast.makeText(Registro.this, "Usuario añadido", Toast.LENGTH_SHORT).show();

                                       }
                                   }, new Consumer<Throwable>() {
                                       @Override
                                       public void accept(Throwable throwable) throws Exception {
                                           Toast.makeText(Registro.this,
                                                   "" + throwable.getMessage(),
                                                   Toast.LENGTH_SHORT).show();
                                       }
                                   }, new Action() {
                                       @Override
                                       public void run() throws Exception {
                                           // loadData();
                                       }
                                   }

                        );
                Intent intent = new Intent(this,menu_principal.class);
                startActivity(intent);
            } else {
                Toast.makeText(Registro.this, R.string.contrasNoCOinciden, Toast.LENGTH_SHORT).show();
            }

        }
    }
}
